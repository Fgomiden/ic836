#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#define	WIDTH	400
#define	HEIGHT	400

int main(int argc, char *argv[]);
void Desenha(void);
void Inicializa (void);

void
Desenha(void)
{
 	glMatrixMode(GL_MODELVIEW); 
	/*Especifica qual matriz é a matriz atual, 
	tem como parâmetro o 
	GL_MODELVIEW que é o valor inicial que 
	aplica operações de matriz subsequentes 
	à pilha de matriz modelview.*/

	glLoadIdentity();
	/*Troca a matriz atual por uma matriz identidade
	(matriz que possui 1 na diagonal principal 
	e zero no restante)*/

  	glClear(GL_COLOR_BUFFER_BIT);
	/*Limpa os buffers para valores predefinidos, 
	tem como parâmetro o
	GL_COLOR_BUFFER_BIT que indica os buffers 
	atualmente ativados para gravação de cores.*/

  	glColor3f(1.0f, 0.0f, 0.0f);	// Especifica a cor corrente: Vermelho
  	
	//glTranslatef(60.0f, 30.0f, 1.0f); // Translada o objeto  
  
	//glRotatef(10.0f, 0.0f, 0.0f, 1.0f); // Rotaciona o objeto  
  
	//glScalef(1.5f, 1.0f, 2.0f); // Escalona o objeto
	
	glBegin(GL_LINES);
	/*Delimita os vértices de um elemento 
	primitivo ou um grupo de elementos
	primitivos semelhantes, tem como parâmetro o
	GL_LINES que trata cada par de vértices como 
	um segmento de linha independente.*/

	/*glVertex2i especifica um vértice*/
		glVertex2i(100,150); 
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
  		glVertex2i(150,150);  

  	glEnd();
	/*Delimita os vértices de um elemento 
	primitivo ou um grupo de elementos
	primitivos semelhantes*/

  	glFlush();
	/*Força a execução de comandos GL em um tempo finito*/  

}

void
Inicializa (void)
{   
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    /*Especifica os valores de vermelho, verde, azul e alfa 
	usados pelo glClear para limpar os buffers de cor*/
}

void
AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	if(h == 0) h = 1;

	glViewport(0, 0, w, h);
	/*Define a janela de visualização,
	tem como parâmetro x, y, width, height
	x, y -> especifica o canto esquerdo inferior 
	do retângulo da janela de visualização, em pixels.
	width,height -> especifica a largura e altura da
	janela de visualização*/

	glMatrixMode(GL_PROJECTION);
	/*Especifica qual matriz é a matriz atual, 
	tem como parâmetro o 
	GL_PROJECTION que Aplica operações de matriz 
	subsequentes à pilha de matriz de projeção.*/

	glLoadIdentity(); //Troca a matriz atual por uma matriz identidade
	
	/*gluOrtho2D define uma matriz de projeção ortográfica 2D,
	tem como parâmetros as coordenadas para o plano de recorte:
	(vertical esquerdo, vertical direito, inferior horizontal, topo horizontal)*/
	if (w <= h) 
		gluOrtho2D (0.0f, 250.0f, 0.0f, 250.0f*h/w);		
	else 
		gluOrtho2D (0.0f, 250.0f*w/h, 0.0f, 250.0f);
}

int
main(int argc, char *argv[])
{
	  glutInit(&argc, argv);	 					// inicia a biblioteca GLUT
     
	 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
	 /*Define o modo de exibição inicial,
	 tem como parâmetros:
	 GLUT_SINGLE -> Máscara de bits para selecionar uma única janela com buffer.
	 GLUT_RGB -> Máscara de bits para selecionar uma janela de modo RGB*/

     glutInitWindowSize(WIDTH, HEIGHT); //Define tamanho da janela inicial
     glutInitWindowPosition(10,10); //Define posição da janela inicial
     glutCreateWindow("LinhasParalelas"); //Cria uma janela top-level
     glutDisplayFunc(Desenha); //Define um callback de exibição para a janela atual 
     glutReshapeFunc(AlteraTamanhoJanela); //Define um callback de remodelagem para a janela atual
     Inicializa();
     glutMainLoop(); //Entra no loop de processamento de eventos GLUT
}
