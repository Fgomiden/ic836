#include <iostream>
#include <string>

#include <Eigen/Core>
#include <Eigen/Dense>

int main(void);

using namespace std;
using namespace Eigen;

int
main(void)
{
	Matrix3i		m1, m2, m3, m4;
	Vector3i		v1, v2, v3, v4;
	int			s1, s2, s3, s4;

	m1 << 1, 2, 3, 
			4, 5, 6, 
			7, 8, 9;
			
	m2 << 1, 2, 3, 
			4, 5, 6, 
			7, 8, 9;

	v1 << 10,20,30;
	v2 << 100, 200, 300;
	s1 = 5;
	s2 = 10;
	s3 = 25;
	cout << "M1" << endl << m1 << endl;
	cout << "M2" << endl << m2 << endl;

	m4 = m1 * s1;
	cout << "M4" << endl << m4 << endl;

	v4 = v1.transpose() * m1;
	cout << "V4" << endl << v4 << endl;

	v4 = m1 * v1;
	cout << "V4" << endl << v4 << endl;

	m4 = m1 * m2 * m3;
	cout << "M4" << endl << m4 << endl;

	return 0;
}
